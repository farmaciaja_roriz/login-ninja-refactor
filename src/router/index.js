import Vue from 'vue';
import Router from 'vue-router';
import formLogin from '@/components/formLogin';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'formLogin',
      component: formLogin,
    },
  ],
});
